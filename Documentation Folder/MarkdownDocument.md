# One hashtag in front makes a header. Sometimes, they also automatically draw lines underneath

## If you add more hashtags, the headers get smaller (this one has two)

### This one has three

#### This one has four

##### This one has five

###### This one has six (this is the maximum allowed)

####### This one has seven (you can see, it broke)




Paragraphs are separated by blank lines.

If you wrap a word in asterisks, it will be in *Italics*, 

wrapping in two asterisks makes it **bold**,

triple italics will make it ***bold AND italics***

backticks will make it  `monospace`. 

You can use SINGLE asterisks in front of lines to make Itemized lists.
They look like:

  * this one
  * that one
  * the other one

You can use left-facing arrows > to make block quotes

> Block quotes are
> written like so.
>
> They can span multiple paragraphs,
> if you like.

You can use triple squiggly ( ~~~ ) delimited blocks to special effect, if you like:

~~~
define function() {
    print "this is some code!";
}
~~~

You can get the same effect by indenting by four spaces

    Four spaced indentation looks like this
    wow, so cool

Here's a numbered list:

 1. first item
 2. second item
 3. third item

Now a nested list:

 1. First, get these ingredients:

      * carrots
      * celery
      * lentils

 2. Boil some water.

 3. Dump everything in the pot and follow
    this recepie:

        find wooden spoon
        uncover pot
        stir
        cover pot
        balance wooden spoon precariously on pot handle
        wait 10 minutes
        Do not bump wooden spoon or it will fall.

You can add hyperlinks to websites with square braces

Here's a link to [a website](http://foo.bar)

Images can be added in with square braces with an exclamation point like so:

![example image](example-image.jpg "image caption text")

The image needs to exist somewhere, accessible to the markdown file, though.

And note that you can backslash-escape any punctuation characters
which you wish to be displayed literally, ex.: \`foo\`, \*bar\*, etc.

Finally, you can use html in markdown if you want to. For example

<img src="example-image.jpg" alt="drawing" width="200"/>